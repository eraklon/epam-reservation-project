/**
 * Script to display and cancel reservations.
 */

"use strict";

(function() {
  
  

$(document).ready(function() {
	
  var pageNumber = 0;
  var totalPages = 0;

  function loadAndDisplayListOfReservations() {
    
    $('.message').hide();
    
    var reservationListTemplateSource = $("#reservation-list-template").html();      // get the template's html source
    var reservationListTemplate = Handlebars.compile(reservationListTemplateSource); // initialize Handlebars template

    $.ajax({
      url : "/api/reservations/?page=" + encodeURIComponent(pageNumber),
      dataType : 'json',
      async : true, 
      cache : false,
      timeout : 5000, 

      data : {},
      success : function(response) {
    	totalPages = response.totalPages;				// get total pages
        var h = reservationListTemplate(response);     // generate HTML from the object using the template
        $("#reservation-list").empty();
        $("#reservation-list").append(h);              // insert the generated HTML into the document
      },
      error : function(XMLHttpRequest, textStatus, errorThrown) {
        console.log("reservation list retrieval failed ... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
        $('#system-error').fadeIn();
      }
    });
  }
  
  $("body").on('click', '.action', function(e) { 
    var reservationId = $(this).attr('reservationId');
    
    $.ajax({
      url : '/api/reservations/' + encodeURIComponent(reservationId),                    
      method: 'DELETE',                           
      async : true,                          
      cache : false,                         
      timeout : 5000,                     

      data : {},                               

      success : function(data, statusText, response) {          
        loadAndDisplayListOfReservations();
      },
                                                
      error : function(XMLHttpRequest, textStatus, errorThrown) {   
        console.log("reservation request failed ... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
        $('#action-error').fadeIn();
      }
    });
  });
  
  //**************** MY FUNCTIONS ***********************************
  
  // Increase @pageNumber on click on nextButton
  $("body").on('click', '.nextButton', function() {
	  if(pageNumber < totalPages - 1) {
		  pageNumber = pageNumber + 1;
		  loadAndDisplayListOfReservations(); // load the table again (next page)
	  }
  });
   
  // Decrease @pageNumber on click on prevButton
  $("body").on('click', '.prevButton', function() {
	  if (pageNumber > 0) { // pageNumber must be 0 or positive
		  pageNumber = pageNumber - 1;
		  loadAndDisplayListOfReservations(); // load the table again (previous page)
	  }
  });
  
  // ******** END OF MY FUNCTIONS *****************************************
  
  loadAndDisplayListOfReservations();
});


})();