package org.training.reservations.repository;

import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.training.reservations.model.Reservation;

/** Throw-away code just for populating test data into the database. */
@Component
public class TestDataPopulator {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private ReservationRepository repository;

    @PostConstruct
    public void init() {
        logger.debug("INIT the db with some test data");

        Calendar from = Calendar.getInstance();
        from.set(2014, 10, 01);
        Calendar to = Calendar.getInstance();
        to.set(2014, 10, 01);
        
        String[] names = {"Joe", "Mary" ,"Peter", "Stacy", "Veronica",
        		"Bob", "James", "Mike", "Lucy", "Adam",
        		"Martin", "Julia", "Mark", "Steve", "David",
        		"Jennifer", "Tereza", "Rebecca","Amy","Carl",
        		"Tom", "Mathew","John","Michael", "William",
        		"Richard", "Charles", "Thomas", "Linda", "Susan",
        		"Pete", "Andrew", "Tara", "Paul", "Tony",
        		"Paula", "Dana", "Jason", "Bruce", "Arnold"};
        
        for (int i = 0; i < names.length; i++) {
            Reservation ss = new Reservation();
            ss.setResourceId((long) i);
            ss.setStartDate(from.getTime());
            ss.setEndDate(to.getTime());
            ss.setOwner(names[i]);
            repository.save(ss);        	
        }
        
    }
}
